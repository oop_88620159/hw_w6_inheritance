/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w6_01;

/**
 *
 * @author acer
 */
public class Duck extends Animal{
    public Duck (String name,String color){
        super(name,color,2);
        System.out.println("Created Duck");
    }
    public void fly(){
        System.out.println("Duck: "+name+" fly!!! ");
    }
     @Override
    public void walk(){
        System.out.println("Duck: "+ name + " walk with "+numberofLegs+" Legs. ");
    }
    @Override
    public void speak(){
        System.out.println("Duck: "+name+" sprak >>>> Grab Grab!!!!!");
    }
}
