/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w6_01;

/**
 *
 * @author acer
 */
public class Cat extends Animal {
    public Cat(String name, String color){
        super(name ,color,4);
        System.out.println("Created Cat");
    }
    @Override
    public void walk(){
        System.out.println("Cat: "+ name + " walk with "+numberofLegs+" Legs. ");
    }
    @Override
    public void speak(){
        System.out.println("Cat: "+name+" sprak >>>> Meow Meow!!!!!");
    }
    
    
}
