/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w6_01;

/**
 *
 * @author acer
 */
public class Animal {
    protected String name;
    protected int numberofLegs = 0;
    protected String color;
    
    public Animal(String name,String color,int numberofLegs){
        System.out.println(" ");
        System.out.println("Animal Created");
        this.name = name;
        this.color = color;
        this.numberofLegs = numberofLegs;
    }
    
    public void walk(){
        System.out.println("Animal walk");
       
    }
    
    public void speak(){
        System.out.println("Animal speak");
        System.out.println("name: "+ this.name+ " color: "+this.color+
                " numberofLegs: "+this.numberofLegs);
    }
    public String getColor(){
        return color;
    }
}
