/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w6_01;

/**
 *
 * @author acer
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal("Ani","White",0);
        animal.speak();
        animal.walk();
        
        Dog dang = new Dog("Dang","BlackWhite");
        dang.speak();
        dang.walk();
        
        Dog to = new Dog("to","Orange");
        to.speak();
        to.walk();
        
        Dog mome = new Dog("mome","BlackWhite");
        mome.speak();
        mome.walk();
        
        Dog bat = new Dog("bat","BlackWhite");
        bat.speak();
        bat.walk();
        
        Cat zero  = new Cat("Zero","Orange");
        zero.speak();
        zero.walk();
        
        Duck zom = new Duck("Zom","Orange");
        zom.speak();
        zom.walk();
        zom.fly();
        
        Duck gabgab = new Duck("gabgab","Black");
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();
        
        System.out.println("--------------TEST OBJECT---------------");
         System.out.println("---------------------------------------");
        System.out.println("dang is animal "+ (dang instanceof Animal));
        System.out.println("dang is Dog "+ (dang instanceof Dog));
        System.out.println("dang is Object "+ (dang instanceof Object));
        
        System.out.println("to is animal "+ (to instanceof Animal));
        System.out.println("to is Dog "+ (to instanceof Dog));
        System.out.println("to is Object "+ (to instanceof Object));
        
        System.out.println("mome is animal "+ (mome instanceof Animal));
        System.out.println("mome is Dog "+ (mome instanceof Dog));
        System.out.println("mome is Object "+ (mome instanceof Object));
        
        System.out.println("bat is animal "+ (bat instanceof Animal));
        System.out.println("bat is Dog "+ (bat instanceof Dog));
        System.out.println("batis Object "+ (bat instanceof Object));
        
        System.out.println("zero is animal "+ (zero instanceof Animal));
        System.out.println("zero is Dog "+ (zero instanceof Cat));
        System.out.println("zero Object "+ (zero instanceof Object));
        
        System.out.println("zom is animal "+ (zom instanceof Animal));
        System.out.println("zom is Dog "+ (zom instanceof Duck));
        System.out.println("zom Object "+ (zom instanceof Object));
        
        System.out.println("gabgab is animal "+ (gabgab instanceof Animal));
        System.out.println("gabgab is Dog "+ (gabgab instanceof Duck));
        System.out.println("gabgab Object "+ (gabgab instanceof Object));
        
        
        System.out.println();
        System.out.println("--------------TEST OBJECT PART2---------------");
        Animal[] animals = {dang,to,mome,bat,zero,zom,gabgab};
        for(int i=0; i< animals.length; i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
            System.out.println(" ");
        }
        
        
        
    }
}
